﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;


   

namespace Vk
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
 //   [Serializable]
    public partial class MainWindow : Window
    {
        private BackgroundWorker bw = new BackgroundWorker();  // создадим фоновый поток
        //public WebBrowser tmpWeb = new WebBrowser(); // создадим веббраузер
       
        public MainWindow()
        {
            InitializeComponent();
            stopButton.IsEnabled = false; // кнопка стоп неактивна при запуске приложения
         
            bw.DoWork+= worker_DoWork;
          
    }
     async   private void worker_DoWork(object sender, DoWorkEventArgs e)    // тут все действия в потоке
        {
            await Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(async () =>
            {
                BackgroundWorker worker = (BackgroundWorker)sender;  // это нужно для остановки потока
            while (!worker.CancellationPending)
            {


                    if (vkKlient.myId == null) // если авторизация еще не проводилась, авторизуемся
                    {

                        await vkKlient.Autorization();     //авторизация
                        this.addLog("Авторизация");
                        await vkKlient.GetGroupAllHash(textBoxTarget.Text);
                        await vkKlient.GetLastPost(textBoxTarget.Text); // получим текст и фото последнего поста  
                    }
                    else
                        await vkKlient.GetLastPost(textBoxTarget.Text); // получим текст и фото последнего поста 
                                                                        //await vkKlient.SentComment("Уже привыкли");
                                                                        //              await vkKlient.GetWallPost(textBoxTarget.Text); // копируем контент с целевой страницы
                    //MessageBox.Show(vkKlient.groupPostText);
                    if (vkKlient.reklam || vkKlient.groupPostText.Contains("погиб") || vkKlient.groupPostText.Contains("скончал") || vkKlient.groupPostText.Contains("умер") ) // проверим не содержится ли в найденом контенте слова реклама и запрещенных слов
                    {
                        this.addLog("Обнаружена Реклама, ufa1.ru или смерть!!");
                    }
                    else
                    {
                        if (vkKlient.countComment > 0 && vkKlient.countComment <= 10 && (DateTime.Now - vkKlient.lastComment).TotalMinutes > 120) // если еще не исчерпан лимит на кол-во комментов и прошло больше 5 минут
                        {
                            if (vkKlient.comments > 3 && vkKlient.groupPostId != vkKlient.lastPostId) // оставлем коммантарии только там где уже есть комменты и постИД не равен тому где уже оставляли коммент
                            {
                               // MessageBox.Show(vkKlient.GenerateComment());
                                vkKlient.SentComment(vkKlient.GenerateComment());
                                vkKlient.lastPostId = vkKlient.groupPostId;
                               
                            }
                            else
                            {
                                this.addLog("Не достаточно комментов или уже оставляли тут коммент!");
                            }
                            
                        }
                        else
                        {
                            this.addLog("Лимит исчерпан или еще не прошло достаточно времени!");
                        }
                      //  this.labelLastComment.Content = "Последний комментарии был: " + vkKlient.lastComment.ToString(); // обновим инфу
                     //   this.labelComment.Content = "Осталось комментов: " + vkKlient.countComment.ToString(); // обновим инфу
                    }
                    // MessageBox.Show(vkKlient.GenerateComment());
                    this.labelLastComment.Content = "Последний комментарии был: " + vkKlient.lastComment.ToString(); // обновим инфу
                    this.labelComment.Content = "Осталось комментов: " + vkKlient.countComment.ToString(); // обновим инфу

                    await Task.Delay(300000); // пауза перед следующей проверкой новых записей на стене 5 минут
                  //   await Task.Delay(7000); // пауза перед следующей проверкой новых записей на стене 5 минут
                    

                    }
           

            }));
        }

        public void openWeb(string _html) // метод веббраузера
        {
          //  webBrowser1.NavigateToString(_html);
         //  webBrowser1.Navigate(_html);
           
           
        }

        public string errors = ""; // переменная для ошибок
       

        public List<Account> listAccount = new List<Account>(); // создадим список из класса Account (будет хранить аккаунты)

      public  VkKlient vkKlient; // создадим экземпляр клиента
        public void addLog(string text) // метод вывода лога
        {
            try
            {
                if (logTextBox.LineCount >= 16) // если много строк уже, очистим перед добавлением новой строки
                    logTextBox.Clear();
                logTextBox.Text += text + "\r\n";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    
      
       
      


        private void usersButton_Click(object sender, RoutedEventArgs e) // кнопка открытия окна с аккаунтами
        {
            try
            {
                accountWindow accountWindow = new accountWindow();  // создадим экземпляр accountWindow (окно аккуантов)
                accountWindow.Owner = this; // сделаем mainWindow родительским окном для окна аккаунтов
                
               
                
                accountWindow.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); /// ввывод ошибки
            }
        }

        private void loadButton_Click(object sender, RoutedEventArgs e) // кнопка загрузки файла конфигурации
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Загрузка конфигурации";
            openFileDialog.Filter = "XML файлы|*.xml";
            
            if (openFileDialog.ShowDialog() == true) // если нажатие ок в диалоге
            {


                FileStream stream = new FileStream(openFileDialog.FileName, FileMode.Open);
              XmlSerializer deserializer = new XmlSerializer(typeof(List<Account>));

                // serializer.Serialize(stream, listAccount);
                listAccount= (List<Account>)deserializer.Deserialize(stream);
                this.addLog("Конфигурация загружена");

                stream.Close();

                /// создадим экземаляр с загруженными параметрами из файла
                vkKlient = new VkKlient(listAccount[0].login, listAccount[0].password, this); // передадим логин и пароль

            }

        }

        private void saveButton_Click(object sender, RoutedEventArgs e) // кнопка сохранения конфигурации
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();

                saveFileDialog.Title = "Сохранение конфигурации";
                saveFileDialog.Filter = "XML файлы|*.xml";

                if (saveFileDialog.ShowDialog() == true) // если нажатие ок в диалоге
                {


                    FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create);
                     XmlSerializer serializer = new XmlSerializer(typeof(List<Account>));

                    serializer.Serialize(stream, listAccount);
                   



                    this.addLog("Конфигурация сохранена");

                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

               
            
        }

         private  void startButton_Click(object sender, RoutedEventArgs e)  // кнопка запуска бота
        {
            try
            {

                 bw.RunWorkerAsync();
              //  MessageBox.Show(vkKlient.GenerateComment());
                startButton.IsEnabled = false; // спрячем кнопку старт
                stopButton.IsEnabled = true;
                   

               


            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

      
       
        private void stopButton_Click(object sender, RoutedEventArgs e) /// конпка стоп
        {
            bw.WorkerSupportsCancellation = true;
            bw.CancelAsync();
            startButton.IsEnabled = true; // сделаем активной кнопку старт
            stopButton.IsEnabled = false;
            this.addLog("Остановка бота!");
        }
    }
}
