﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Vk
{
    /// <summary>
    /// Логика взаимодействия для CookieWindow.xaml
    /// </summary>
    public partial class CookieWindow : Window
    {
        public VkKlient _vk;
        public CookieWindow( VkKlient vk)
        {
            _vk = vk;
            InitializeComponent();
        }

        private void buttonAddCookie_Click(object sender, RoutedEventArgs e)
        {

            _vk.tempCookie = textBoxRemixsid.Text; // передадим значение в вкклиент
            _vk.myId = "509974533";
            this.Close();
        }
    }
}
