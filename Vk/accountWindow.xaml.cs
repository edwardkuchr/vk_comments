﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;



namespace Vk
{
    /// <summary>
    /// Логика взаимодействия для accountWindow.xaml
    /// </summary>
   
    public partial class accountWindow : Window
    {
       
        public accountWindow()
        {
            InitializeComponent();
            
        }
        //public List<Account> listAccount = new List<Account>(); // создадим список из класса Account (будет хранить аккаунты)
        public void addLoginButton_Click(object sender, RoutedEventArgs e) //реакция на кнопку добавить
        {
            try
            {
                MainWindow main = this.Owner as MainWindow;

                if (main != null) // проверим, существует ли родительское окно
                {
                    if (loginTextBox.Text != "" && passwordTextBox.Text != "") //проверка на пустоту строк
                    {
                        Account itemAccount = new Account(loginTextBox.Text, passwordTextBox.Text);

                         main.listAccount.Add(itemAccount); // добавим в список данные аккаунта
                       // this.listAccount.Add(itemAccount); // добавим в список данные аккаунта


                        main.addLog("Добавлен аккаунт"); //вывод в окно лога

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при добавления аккаунта: "+ ex.Message);
            }
       
        }

        private void accountDataGrid_Loaded(object sender, RoutedEventArgs e) // заполнение таблицы
        {

            try
            {
                MainWindow main = this.Owner as MainWindow;
                if (main != null)
                {
                    accountDataGrid.ItemsSource = main.listAccount; // заполним таблицу формы с аккаунтами
                   // accountDataGrid.ItemsSource = this.listAccount; // заполним таблицу формы с аккаунтами
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при заполнении таблицы аккаунтов: " + ex.Message);
            }
        }

        private void delLoginButton_Click(object sender, RoutedEventArgs e) // действие на кнопу удаления
        {
            try
            {
                MainWindow main = this.Owner as MainWindow;
                if (main != null && accountDataGrid.SelectedIndex != -1) // проверка что рордительское окно существует, и выбрана строка
                {

                      main.listAccount.RemoveAt(accountDataGrid.SelectedIndex); //удаляем из коллекции выделенный индекс
                   // this.listAccount.RemoveAt(accountDataGrid.SelectedIndex); //удаляем из коллекции выделенный индекс
                    main.addLog("Удален аккаунт"); //вывод в окно лога
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при удалении аккаунта: " + ex.Message);
            }
            
        }
    }
}
