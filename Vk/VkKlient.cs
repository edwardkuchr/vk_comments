﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Windows;


namespace Vk
{

    public class VkKlient

    {
        VkKlient()
        { }

        public VkKlient(string log, string pass, MainWindow _mainWindow)  //конструктор для вк клиента

        {
            login = log;// переменная логин
            password = pass; // переменная пароля
            mainWindow = _mainWindow; // ссылка на главное окно
        }

        MainWindow mainWindow;

      
        HttpRequestMessage request = new HttpRequestMessage();
        //HttpResponseMessage response = new HttpResponseMessage();

       static CookieContainer cookieContainer = new CookieContainer(); // создадим контейнер куки 
       public string tempCookie; // через нее будет передавать заголовок в куки из окна CoolieWindow

        static HttpClientHandler handler = new HttpClientHandler { CookieContainer = cookieContainer };
        HttpClient client = new HttpClient(handler);
        public string html; // перменная с исходным кодом


        string login;// переменная логин
        string password;// переменная пароля

        string ip_h; // переменные из html кода для авторизации
        string lg_h; // переменные из html кода для авторизации
        public string myId;// мой Id
        public string groupId; // id группы где нужно создать пост
        public string myPost_hash = ""; // хэш для отправки сообщений на стену
        public string myTime_hash = ""; // хэш который меняется постоянно при обновлении страницы
        public string groupPost_hash = ""; // хэш  группы для отправки сообщений на стену
        public string groupPostText = ""; // текст последнего поста в группе
        public string groupPostPhoto = ""; // фото последнего поста в группе
        public string groupPostId = ""; // ИД поста в группе
        public int comments; // количество комментариев в посте
        public string lastPostId;// тут ID последнего поста где оставляли коммент
        public DateTime lastComment; // дата последнего комментарияъ
        public DateTime firstComment;// когда написан первый коммент
        public int countComment=10; // Сколько осталось коммантариев в сутки
        public bool reklam;// обнаружена реклама или нет
      //  public DateTime dataToday; // сегодняшняя дата



        CookieCollection responseCookies; //коллекция куки





        async public Task Autorization() //метод авторизации

        {
            try  //в этой ветке получаем  ip_h, lg_h они нужны для получения hash (
            {

                myId = ""; // обнулим ID
                //добавим заголовки которые будет содержать запрос
                client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                //             client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip,deflate, br");
                client.DefaultRequestHeaders.Add("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
                client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36 OPR/51.0.2830.55");



                //    var request = client.GetStringAsync("https://vk.com");
                //   request.GetAwaiter();
                //    html = request.Result;
                request.RequestUri = new Uri("https://vk.com");

                request.Method = HttpMethod.Get;










                html = await client.GetStringAsync(request.RequestUri);
                responseCookies = cookieContainer.GetCookies(request.RequestUri);//.Cast<Cookie>(); //эта строка присваивает куки после запроса



                Regex regex = new Regex(@"ip_h*\W*\s*(\w+)");
                Match match = regex.Match(html);
                ip_h = match.Groups[1].Value;
                Regex regex2 = new Regex(@"lg_h*\W*\w*\W*(\w+)");
                Match match2 = regex2.Match(html);
                lg_h = match2.Groups[1].Value;
                // MessageBox.Show(html);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " Ошибка в методе получения данных ip_h, lg_h!");

            }
            // getForAutorization(); // получим сперва ip_h, lg_h
            try
            {


                //добавим заголовки которые будет содержать запрос
                client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                // client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
                client.DefaultRequestHeaders.Add("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
                client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36 OPR/51.0.2830.55");



                login = HttpUtility.UrlEncode(login);  // перекодируем логин
                password = HttpUtility.UrlDecode(password);  // перекодируем пароль
                request.RequestUri = new Uri("https://login.vk.com/?act=login");



                request.Method = HttpMethod.Post;





                HttpContent content = new StringContent("act=login&role=al_frame&expire=&recaptcha=&captcha_sid=&captcha_key=&_origin=https%3A%2F%2Fvk.com&ip_h=" + ip_h + "&lg_h=" + lg_h + "&email=" + login + "&pass=" + password, Encoding.UTF8, "application/x-www-form-urlencoded");




                HttpResponseMessage response = await client.PostAsync(request.RequestUri, content);
                content = response.Content; // получим исходный код страницы для получения myId и других Hash

                html = await content.ReadAsStringAsync();


                responseCookies = cookieContainer.GetCookies(request.RequestUri);//.Cast<Cookie>(); //эта строка присваивает куки после запроса

               
 //               else  // если нет каптчи, получаем все остальное
 //               {
                    Regex regex = new Regex(@"id+\W+(\d+)");  //извлечем myId
                    Match match = regex.Match(html);
                    myId = match.Groups[1].Value;
                    if (myId != "") // проверка получили id или нет
                    {
                        //MessageBox.Show(myId);
                        mainWindow.addLog("Авторизация успешна");
                        // mainWindow.myAcInfo.Header = "Данные аккаунта";
                        mainWindow.labelMyId.Content = "ID=" + myId;

                        GetMyAllHash("https://vk.com/id" + myId); // запустим метод получения всех hash
                    }
                    else
                    {
                        mainWindow.addLog("ID не получен");
                        mainWindow.labelMyId.Content = "ID=" + myId;
                    }
                if (html.Contains("parent.onLoginReCaptcha"))  // отследим требуется ли каптча
                {
                    mainWindow.addLog("АААА Словили каптчу!!");
                    CookieWindow cookieWindow = new CookieWindow(this); // создадим окно с вводом куки и передадим туда ссылку на вкклиент
                    cookieWindow.ShowDialog();
                    cookieContainer.Add(new Cookie("remixsid", tempCookie, "/", ".vk.com"));
                 //   myId = "509974533";
                    mainWindow.labelMyId.Content = "ID=" + myId;

                    GetMyAllHash("https://vk.com/id" + myId); // запустим метод получения всех hash
                }
                //               }
                //    this.CheckAutorization(); // после авторизации проверим авторизацию
                // MessageBox.Show(myId);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " Ошибка в методе авторизации!");

            }



        }

        async public void GetMyAllHash(string uri) // метод получения всех моих hash 
        {
            try
            {
                request.RequestUri = new Uri(uri);//"https://vk.com/id" + id); // отправим гет запрос на получения моей страницы

                request.Method = HttpMethod.Get;
                html = await client.GetStringAsync(request.RequestUri);

                /////////////////////// если получили myId, полчим все Hash которые нам нужны




                Regex regex = new Regex(@"post_hash+\W+(\w+)");  //извлечем post_hash
                Match match = regex.Match(html);
                myPost_hash = match.Groups[1].Value;


                if (myPost_hash != "") // проверка получили post_hash или нет
                {

                    mainWindow.addLog(" - Мой post_hash получен!");
                    mainWindow.labelPostHash.Content = "MyPost_Hash=" + myPost_hash;

                }
                else
                {
                    mainWindow.addLog(" - Мой post_hash не получен!!");

                }

                Regex regex2 = new Regex(@"timehash+\W+(\w+[_]*)");  //извлечем timehash (нужен для вставки картинок)
                Match match2 = regex2.Match(html);
                myTime_hash = match2.Groups[1].Value;
                mainWindow.labelTimeHash.Content = "MyTimeHash=" + myTime_hash;
                if (myTime_hash != "") // проверка получили timehash или нет
                {

                    mainWindow.addLog(" - Мой timehash получен!");

                }
                else
                {
                    mainWindow.addLog(" - Мой timehash не получен!!");

                }
                //MessageBox.Show(time_hash);
                //  GetWallText("4r5t6y");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " Ошибка в методе получения всех  Моих Hash!");

            }
        }
        async public Task GetGroupAllHash(string uri) // метод получения всех hash  группы куда будем посты делать
        {
            try
            {
                request.RequestUri = new Uri(uri);//"https://vk.com/id" + id); // отправим гет запрос на получения моей страницы

                request.Method = HttpMethod.Get;
                html = await client.GetStringAsync(request.RequestUri);

                groupId = "";

                Regex regex0 = new Regex(@"public_id+\W+(\d+)");  //извлечем  Id группы
                Match match0 = regex0.Match(html);
                groupId = match0.Groups[1].Value;
                if (groupId == "") // проверка получили id или нет
                {
                    //MessageBox.Show(myId);
                    mainWindow.addLog("ID группы не получен");

                }
                else
                {

                    mainWindow.labelGroupId.Content = "GroupID=" + groupId;


                    /////////////////////// если получили Id группы, полчим все Hash которые нам нужны


                }

                Regex regex = new Regex(@"post_hash+\W+(\w+)");  //извлечем post_hash
                Match match = regex.Match(html);
                groupPost_hash = match.Groups[1].Value;


                if (groupPost_hash != "") // проверка получили post_hash или нет
                {

                    mainWindow.addLog(" - post_hash Группы получен!");
                    mainWindow.labelGroupPostHash.Content = "GroupPost_Hash=" + groupPost_hash;

                }
                else
                {
                    mainWindow.addLog(" - post_hash Группы не получен!!");

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " Ошибка в методе получения всех Hash гоуппы!");

            }
        }
        // Поиск последнего поста в группе (текста и картинки)
        async public Task GetLastPost(string uriGroup)
        {
            try
            {

                groupPostPhoto = ""; // обнулим
                groupPostText = "";// обнулим
                groupPostId = ""; // обнулим
                request.RequestUri = new Uri(uriGroup); // отправим гет запрос на получения кода целевой страницы

                request.Method = HttpMethod.Get;
                html = await client.GetStringAsync(request.RequestUri);

                html = html.Replace("<br>", "\n"); // заменим тег переноса на пернос Си
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument(); // создаем экземпляр html парсера

                doc.LoadHtml(html);


                

                HtmlNodeCollection photo = doc.DocumentNode.SelectNodes("//div[@class='_post post page_block all own deep_active']");  // поиск тега с постами
                string temp = ""; // временная с найденным исходным кодом
                if (photo == null) // если по одному шаблону не нашли посты, поищем по другому (не везде этот шаблон используется)
                {
                    photo = doc.DocumentNode.SelectNodes("//div[@class='_post post page_block all own post--with-likes deep_active']");
                }
                foreach (HtmlNode item in photo)
                {
                    if (item.InnerHtml.Contains("Написать комментарий")) //  ищем последний пост где можно оставлять комментарии
                    {
                        temp = item.InnerHtml;
                        if (temp.Contains(">Реклама<") || temp.Contains("ufa1.ru")) // По дурацки, но так обнаружим тег Реклама или новость с сайта ufa1.ru
                        {
                            reklam = true;
                        }
                        else
                        {
                            reklam = false;
                            
                        }
                        break;
                    }
                }
                // MessageBox.Show(temp);
                Regex regexGroupPostId = new Regex("div.id=.wpt(.*?)\"");  //извлечем groupPostId
                Match matchGroupPostId = regexGroupPostId.Match(temp);
                groupPostId = matchGroupPostId.Groups[1].Value;
             //   MessageBox.Show(groupPostId);



                Regex regexText = new Regex(@"wall_post_text..([\s\S]*?)\<");  //извлечем текс
                Match matchText = regexText.Match(temp);
                groupPostText = matchText.Groups[1].Value;
                if (groupPostText == "")  // если текста в посте нет вывдем сооьбщение
                {
                    mainWindow.addLog("Текста  в посте нет!!");
                }
                // MessageBox.Show(groupPostText);
             

                Regex regexComments = new Regex(@"Комментарий""[\s\S]*?like_button_count"">([0-9]*)");  //извлечем количество комментариев
                Match matchComments = regexComments.Match(temp);
                if (matchComments.Groups[1].Value =="") // если количество комментов не спарсилось, то пусть они равны 0
                {
                    comments = 0;
                }
                else
                comments = Convert.ToInt32(matchComments.Groups[1].Value);
                
                // MessageBox.Show(comments.ToString());

                Regex regexPhoto = new Regex(@"background-image:.url\((.*?)\)");  //извлечем ссылку на фото

                Match matchPhoto = regexPhoto.Match(temp);
                groupPostPhoto = matchPhoto.Groups[1].Value;  // получаем ссылку на картинку

          //      MessageBox.Show(groupPostPhoto);
                if (groupPostPhoto == "")  // если фотка не существут вывдем сооьбщение
                {
                    mainWindow.addLog("Картинки в посте нет!!");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " Ошибка в методе поиска последней записи в группе");
            }





        }
        async public Task SentComment(string text)
        {
            try
            {
                
                request.Method = HttpMethod.Post;
                HttpContent content;
                HttpResponseMessage response;


                /////////////////////////тут происходит нажатие кнопки отправить запись с картинкой
                request.RequestUri = new Uri("https://vk.com/al_wall.php");
                request.Method = HttpMethod.Post;


                content = new StringContent("Message=" + HttpUtility.UrlEncode(text)+ "&act=post&al=1&from=&from_oid="+myId+"&hash="+groupPost_hash+"&ref=wall_page&reply_to="+groupPostId+"&reply_to_msg=&reply_to_user=0&start_id=2376537&type=own", Encoding.GetEncoding(1251), "application/x-www-form-urlencoded");



                response = await client.PostAsync(request.RequestUri, content);
                content = response.Content; // получим исходный код страницы 

                html = await content.ReadAsStringAsync();


                mainWindow.addLog("Создание коммента... " + response.StatusCode.ToString());
                if (countComment==10) // если комментов сегодня еще не оставляли
                {
                    firstComment = DateTime.Now; // пропишим дату первого коммента
                    lastComment = DateTime.Now; // пропишим дату последнего коммента
                }
                else
                {
                    lastComment = DateTime.Now; // если комменты сегодня уже были, просто пропишим дату последнего коммента
                }
                countComment =countComment-1; // после создания коммента уменьшим остаток
                //   targetText = ""; // обнулим после созадния

            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " Ошибка в методе отправки коммента!");
            }


        }

        public string GenerateComment() // метод генерации комментария
        {
            Random rnd0 = new Random();// генератор букв алфавита.
            char bukva = (char)rnd0.Next(0x0061, 0x007A); // любой символ от а...z
            Random rnd1 = new Random(); // генератор количества точек
            int kol = rnd1.Next(0,6); // от 1 до 6
            string tochki = "";
            for (int i = 0; i <= kol;i++)
            {
                tochki = tochki+".";
            }
            Random rnd = new Random(); // создадим генератор
            int value = rnd.Next(1, 15); // диапазон от и до
            Dictionary<int, string> dict = new Dictionary<int, string>();
            
            dict.Add(1,"Да уж..");
            dict.Add(2, "Это где?");
            dict.Add(3, "😒 капец какойто");
            dict.Add(4, "Ужас какой-то");
            dict.Add(5, "Как же так");
            dict.Add(6, "не надо так 😳 ");
            dict.Add(7, "😩 ого");
            dict.Add(8, "ничего себе");
            dict.Add(9, "Вот дела");
            dict.Add(10, "нифига себе");
            dict.Add(11, "офигеть!");
            dict.Add(12, "Ну ебошкин кот, как так");
            dict.Add(13, "Куда катится мир");
            dict.Add(14, "Полностью согласна");
            dict.Add(15, "Вы совершенно правы");

            return dict[value]+tochki+bukva; // вернуть рандомный ответ
        }
    }
    }
